/* Variables */
var socket = io();
var loginButton = $('#login-button');
var userPage = $('#user-page');
var thisUser;
var clientUsers;

socket.on('login',        function(users, user) { onUserLogin(users, user) } );

socket.on('currentLogin', function(user)        { onCurrentLogin(user) });

socket.on('ready',        function(user)        { onUserReady(user) } );

socket.on('disconnect',   function(user)        { onUserDisconnect(user) } );

function onUserLogin(users, user) {

    userPage.empty();

        for(var u in users) {

            if(users[u].user_id !== thisUser.user_id) {
                userPage.append('<div id="user_id_' + users[u].user_id + '" class="col s12 m5 l3"><div class="card blue-grey darken-1 z-depth-4"><div class="card-content white-text center-align"><span class="card-title">'+ users[u].username +'</span></div><div class="card-action center-align"><a class="disabled waves-effect waves-light btn-floating z-depth-2"><i class="material-icons left">mic_none</i></a></div></div></div>');
            }
            else {
                userPage.append('<div id="user_id_' + users[u].user_id + '" class="col s12 m5 l3"><div class="card blue-grey darken-1 z-depth-4"><div class="card-content white-text center-align"><span class="card-title">'+ users[u].username +'</span></div><div class="card-action center-align"><a id="ready_button_' + users[u].user_id + '" class="waves-effect waves-light btn-floating z-depth-2"><i class="material-icons left">mic_none</i></a></div></div></div>');
            }

        }

    clientUsers = users;
    $('#ready_button_' + thisUser.user_id).click(function(){
        socket.emit('ready', users[thisUser.user_id]);
    });

}

function onCurrentLogin(user) {

    thisUser = user;

};

function onUserReady(user) {

    clientUsers[user.user_id].ready = user.ready;

};

function onUserDisconnect(user) {

    delete clientUsers[user.user_id];
    $("#user_id_" + user.user_id).remove();

};

$(document).ready(function(){

    $('#modal').openModal({
        dismissible: false
    });

});

loginButton.click(function(){

    socket.emit('login', $('#username').val());

});
