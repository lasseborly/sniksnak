# SnikSnak
#### A poor mans podcast suite.

***

### Dependecies
* To build server dependencies run in project root

    `npm install`

* To build front-end dependencies run in project `/public`

    `bower install`
    
***

### Run
* To run server

    `npm start`

* To run tests

    `npm test`

    **!!Remember to have the server running when executing the tests!!**
