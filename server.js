var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

var users = {};
var user_id = 0;

server.listen(port, function () {
    console.log('Server listening at port %d', port);
});

app.use(express.static(__dirname + '/public'));

io.on('connection', function(socket){
    console.log('a user connected');

    socket.on('login', function(username)   { onUserLogin(socket, username) });

    socket.on('ready', function(user)       { onUserReady(socket, user) });

    socket.on('disconnect', function()      { onUserDisconnect(socket) });

});

function onUserLogin(socket, username) {

    var user = {user_id: user_id ,username: username, ready: 0};
    socket.user_id = user_id;
    users[user_id] = user;
    user_id++;

    console.log(username + ' has logged in');

    socket.emit('currentLogin', user);
    io.emit('login', users, user);

};

function onUserReady(socket, user) {

    if (user.ready === 0) {
        users[user.user_id].ready = 1;
        console.log(users[user.user_id].username + " is ready");
    }
    else if (user.ready === 1) {
        users[user.user_id].ready = 0;
        console.log(users[user.user_id].username + " is not ready");
    }

    io.emit('ready', users[user.user_id]);

};

function onUserDisconnect(socket) {

    console.log(users[socket.user_id].username + " disconnected!");
    io.emit("disconnect", users[socket.user_id]);
    delete users[socket.user_id];

};
