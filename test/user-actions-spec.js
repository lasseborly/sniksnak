var should = require('should');
var io = require('socket.io-client');

var socketURL = 'http://localhost:3000';

var options ={
    transports: ['websocket'],
    'force new connection': true
};

var client1;
var client2;
var user1;
var user2;
var clientUsers;

describe("SnikSnak Server user actions",function(){

    it('Should instantiate the user so the client knows who it is', function(done){

        client1 = io.connect(socketURL, options);
        client1.emit('login', 'TestUser1');

        client1.on('currentLogin', function(user){
            user1 = user;

            user1.should.not.be.empty();
            user1.ready.should.equal(0);
            user1.user_id.should.equal(0);
            user1.username.should.equal('TestUser1');
            done();
        });

        client1.on('login', function(users, user){
            clientUsers = users;
        });

    });

    it('Should have +1 user after a user has logged in', function(done){

        var firstUserCount;
        client2 = io.connect(socketURL, options);
        client2.emit('login', 'TestUser2');

        client2.on('currentLogin', function(user){
            user2 = user;
            firstUserCount = Object.keys(clientUsers).length;
        });


        client2.on('login', function(users, user){

            clientUsers = users;

            var secondUserCount = Object.keys(clientUsers).length;

            (secondUserCount).should.be.above(firstUserCount);
            done();
        });


    });

    it('Should change a users ready state', function(done){

        var firstUserReadyState = user1.ready;

        client1.emit('ready', user1);

        client2.on('ready', function(user){
            clientUsers[user.user_id].ready = user.ready;
            var secondUserReadyState = clientUsers[user.user_id].ready;

            (firstUserReadyState).should.not.equal(secondUserReadyState);
        });
        done();

    });
});
